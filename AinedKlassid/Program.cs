﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AinedKlassid
{
    class Program
    {
        static void Main(string[] args)
        {
            // meil vaja tuvastada, mis ainet mis klassis õpetatakse
            // matemaatikat 3, 4 ja viiendas
            // 4. klasssis matemaatikat, eesti keelt ja joonistamist

            new Klass("1a", "Esimene a");
            new Klass("2b", "Teine b");
            new Klass("1c", "Esimene c");
            new Klass("4a", "Neljas a");

            foreach (var x in Klass.Klassid.Values) Console.WriteLine(x.Nimetus);

            new Aine("esta") { Nimetus = "Eesti keel" };
            new Aine("mata") { Nimetus = "Matemaatika" };
            new Aine("füssa") { Nimetus = "Füüsika" };
            new Aine("jonna") { Nimetus = "Joonistamine" };
            new Aine("lalla") { Nimetus = "Laulmine" };

            foreach (var x in Aine.Ained.Values) Console.WriteLine(x.Nimetus);

            //KusMida("esta", "1a");
            //KusMida("esta", "2b");
            //KusMida("esta", "1c");
            //KusMida("esta", "4a");
            //KusMida("mata", "2b");
            //KusMida("mata", "4a");
            //KusMida("füssa", "4a");

            KusMida("esta", "1a", "2b", "1c", "4a" );
            KusMida("mata", "2b", "4a");
            KusMida("füssa", "4a");

            Console.WriteLine("Ained ja kus neid õpetatakse:"  );
            foreach (var x in Aine.Ained.Values)
            {
                Console.WriteLine("Õppeainet {0} õeptatakse järgmistes klassides:", x.Nimetus);
                foreach (var y in x.KusÕpetatakse) Console.WriteLine(y.Nimetus);
                Console.WriteLine();
            }




        }

        static void KusMida(string aineKood, string klassiKood)
        {
            Aine.Ained[aineKood].KusÕpetatakse.Add(Klass.Klassid[klassiKood]);
            Klass.Klassid[klassiKood].MisAined.Add(Aine.Ained[aineKood]);
        }

        // sellele meetodile saab anda ette klasside koodid massiivina
        // aga selle massiivina antakse ka loetelu parameeter (mitu samatüüpi väärtust)
        // seda lubab võtmesõna params seal string[] ees
        static void KusMida(string ainekood, params string[] klassiKoodid)
        {
            foreach (var x in klassiKoodid) KusMida(ainekood, x);
        }

    }

    class Aine
    {
        // siia paneme kõik ained kirja
        public static Dictionary<string, Aine> Ained = new Dictionary<string, Aine>();


        public string Kood; // näiteks esta ja mata ja füssa
        public string Nimetus; // näiteks Eesti keel, Matemaatika jne
        // siia paneme kirja, kus klassis mida õpetatakse
        public List<Klass> KusÕpetatakse = new List<Klass>();
        // lisaks võib ainel olla veel igasuguseid tunnuseid, mida me praegu kirja ei pane

        // konstruktor loob aine ja pistab sinna dictionary
        public Aine(string kood)
        {
            this.Kood = kood;
            Ained.Add(kood, this);
        }

    }
    class Klass  // sellega teeme umbes samamoodi
    {
        public static Dictionary<string, Klass> Klassid = new Dictionary<string, Klass>();
        
        public string Kood; // näiteks "1a", "2b" jne
        public string Nimetus; // näiteks "esimene a", "teine b" jne
        // klassil võib olla veel terve hulk tunnuseid, mida me praegu kirja ei pane
        // näiteks, kus ruumis on koduklass või kas on eksamiaine vms

        public List<Aine> MisAined = new List<Aine>();

        public Klass (string kood, string nimetus)
        {
            Kood = kood; Nimetus = nimetus;
            Klassid.Add(kood, this);
        }

    }
}
